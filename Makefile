#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lpousse <lpousse@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/06/10 11:46:50 by lpousse           #+#    #+#              #
#    Updated: 2016/09/27 16:52:30 by lpousse          ###   ########.fr        #
#                                                                              #
#******************************************************************************#

NAME = libftprintf.a
SRC_NAME = ft_printf.c parsing.c int_converter.c uint_converters.c char_converters.c str_converters.c pointer_converter.c
SRC_PATH = ./
OBJ_PATH = obj/
INC_PATH = ./ libft/includes/

CC = gcc
CFLAGS = -Wall -Wextra -Werror
CPPFLAGS = $(addprefix -I ,$(INC_PATH))
LDFLAGS = -L libft
LDLIBS = -lft

OBJ_NAME = $(SRC_NAME:.c=.o)
SRC = $(addprefix $(SRC_PATH),$(SRC_NAME))
OBJ = $(addprefix $(OBJ_PATH),$(OBJ_NAME))

all: mklib $(NAME)

$(NAME): $(OBJ)
	mv libft/libft.a $(NAME)
	ar rc $(NAME) $(OBJ)
	ranlib $(NAME)

mklib:
	make -C libft

$(OBJ_PATH)%.o: $(SRC_PATH)%.c $(addsuffix *.h,$(INC_PATH))
	@mkdir -p $(OBJ_PATH)
	$(CC) $(CFLAGS) $(CPPFLAGS) -o $@ -c $<

clean:
	rm -fv $(OBJ)
	@rmdir $(OBJ_PATH) 2> /dev/null || true
	make -C libft clean

fclean: clean
	rm -fv $(NAME)
	make -C libft fclean

wclean:
	rm $(SRC_PATH)*~ $(addsuffix *~,$(INC_PATH))

re: fclean all

norme:
	norminette $(SRC) ./*.h

.PHONY: all clean fclean re mklib norme